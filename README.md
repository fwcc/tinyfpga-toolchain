# TinyFPGA Toolchain

>**Important Information:**
>
>**Operating System: Ubuntu 23.04**
>
>**Python version 3.11/3.11.4**
>
>**This is a Linux only solution on Windows and Mac this may not work**

**!!!WANRNING!!!**

**WITHOUT Python 3.11 INSTALLED THE AUTOMATED PATCH WON'T WORK AND YOU NEED TO DO IT MANUALY**

## Getting started

Download this repository and follow these steps:
- open a new terminal
- install git using the following command: `sudo apt install git`
- type: `git clone https://codebase.helmholtz.cloud/fwcc/tinyfpga-toolchain.git && cd tinyfpga-toolchain`

- continue by typing: `bash setup.sh`
- if you have a fpga withot metadata or (a TinyFPGA from the batch of July, 2023)type: `cd Patch && bash apply-patches.sh`

## Getting started with the TinyFPGA
- connect your TinyFPGA to your computer
- get access to the 'dialout' group by using this command: `sudo usermod -a -G dialout YOUR_USERNAME`
- type: `tinyprog`
    - the expected output looks something like this:
    
    > TinyProg CLI
    > 
    >  ------------
    >
    > Using device id 1d50:6130
    >
    > Only one board with active bootloader, using it.

## Setting up your environment
- go to the folder you want to work in
- type `apio init --board TinyFPGA-BX`
- verify the content by typing `cat apio.ini`
    - the content should look like this:
    > [env]
    >
    > board = TinyFPGA-BX

## Running your first programm on the TinyFPGA-BX
- coppy the example file in your working directory with the apio.ini file
- type: `apio verify` to check for errors in your verilog file (*.v)
    - the end of the output should look something like this:
    >
    >============== [SUCCESS] Took 0.31 seconds ==============

- to compile your code type `apio build`
    - the end of the output should look something like this:
    >============== [SUCCESS] Took 1.66 seconds ==============

- to upload and run your code on the TinyFPGA-BX type `tinyprog -p hardware.bin` or `tinyprog --program-image hardware.bin`
- **ATTENTION: IF YOU REBOOT OR RECONNECT YOUR TinyFPGA-BX YOUR CONFIGURATION MIGHT NOT RUN TO FIX THIS CONNECT YOUR TinyFPGA-BX TO AN EXTERNAL POWERSOURCE WITH ONE OF THE `G` PINS ON GROUND AND THE `VIN` PIN TO 3.3V ON YOUR POWERSOURCE**

- **UPDATE: You can also type `tinyprog -b` to re run your configuration**

- **The TinyFPGA-BX only boots from the 'userimage' section of the spi flash**

