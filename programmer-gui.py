import sys
import tkinter as tk
from tkinter import filedialog, messagebox
import tinyprog
from tkinter import *
import json

def open_file_dialog():
    try:
        file_path = filedialog.askopenfilename(filetypes=[("Bin Files", "*.bin"),('Hex Files', '*.hex')],)
        if file_path:
            file_path_entry.delete(0, tk.END)
            file_path_entry.insert(0, file_path)
    except IndexError:
        messagebox.showerror('Attention','No')

def button2_action():
    try:
        selected_file = file_path_entry.get()
        port = tinyprog.get_ports("1d50:6130")[0]
        def progress(info):
            if isinstance(info, str):
                print("    " + str(info))
        def check_if_overwrite_bootloader(addr, length, userdata_range):
            ustart = userdata_range[0]
            uend = userdata_range[1]

            if addr < ustart or addr + length >= uend:
                print("")
                print("    !!! WARNING !!!")
                print("")
                print("    The address given may overwrite the USB bootloader. Without the")
                print("    USB bootloader the board will no longer be able to be programmed")
                print("    over the USB interface. Without the USB bootloader, the board can")
                print("    only be programmed via the SPI flash interface on the bottom of")
                print("    the board")
                print("")
                retval = strict_query_user("    Are you sure you want to continue? Type in 'yes' to overwrite bootloader.")
                print("")
                return retval
            return True
        if selected_file:
            with port:
                fpga = tinyprog.TinyProg(port, progress)
                prog = tinyprog.TinyProg(port.ser)
                bitstream = fpga.slurp(selected_file)
                print(fpga.is_bootloader_active())
                if check_if_overwrite_bootloader(prog.meta.userimage_addr_range()[0], len(bitstream),fpga.meta.userimage_addr_range()):
                    print('writing to userimage')
                    prog.program_bitstream(addr=prog.meta.userdata_addr_range()[0], bitstream=bitstream)
                    fpga.boot()

    except IndexError:
        messagebox.showerror('Attention','No port was specified and no active bootloaders found.\nActivate bootloader by pressing the reset button on your fpga.')

def button3_action():
    try:
        port = tinyprog.get_ports("1d50:6130")[0]
        with port:
            prog = tinyprog.TinyProg(port.ser)
            prog.boot()
    except IndexError:
        messagebox.showerror('Attention','No port was specified and no active bootloaders found.\nActivate bootloader by pressing the reset button on your fpga.')

def button4_action():
    try:
        port = tinyprog.get_ports("1d50:6130")[0]
        with port:
            prog = tinyprog.TinyProg(port.ser)
            messagebox.showinfo("Metadata", json.dumps(prog.meta.root, indent=2))
    except IndexError:
        messagebox.showerror('Attention','No port was specified and no active bootloaders found.\nActivate bootloader by pressing the reset button on your fpga.')

class Checkbar(Frame):
   def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
      Frame.__init__(self, parent)
      self.vars = []
      for pick in picks:
         var = IntVar()
         chk = Checkbutton(self, text=pick, variable=var)
         chk.pack(side=side, anchor=anchor, expand=YES)
         self.vars.append(var)

   def state(self):
      return map((lambda var: var.get()), self.vars)

root = tk.Tk()
root.title("TinyFPGA Programmer")

frame = tk.Frame(root)
frame.pack(pady=10)

label = tk.Label(frame, text="File Path:")
label.grid(row=0, column=0, padx=10)

file_path_entry = tk.Entry(frame, width=50)

file_path_entry.grid(row=0, column=1, padx=10, pady=10, sticky="ew")

button1 = tk.Button(root, text="Choose bitstream File", command=open_file_dialog)
button2 = tk.Button(root, text="Load bitstream to FPGA", command=button2_action)
button3 = tk.Button(root, text="Reboot the FPGA", command=button3_action)
button4 = tk.Button(root, text="Get FPGA info", command=button4_action)

button1.pack(side=tk.LEFT,padx=10, pady=10)
button2.pack(side=tk.LEFT,padx=10, pady=10)
button3.pack(side=tk.LEFT,padx=10, pady=10)
button4.pack(side=tk.LEFT,padx=10, pady=10)

root.mainloop()
