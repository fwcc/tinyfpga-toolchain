#!/bin/bash
usr_input="$1"
xusr_input="$2"
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
if [ "$usr_input" = "upload" ]; then
        command=$(apio verify)
	item="SUCCESS"
	if grep -q "$item" <<< "$command"; then
		command=$(apio build)
		command=$(tinyprog -p hardware.bin)
		item="Success"
		if grep -q "$item" <<< "$command"; then
			echo -e "${GREEN}⬤${NC} your configuration has been uploaded"
		else
			echo -e "${RED}⬤${NC} there has been an error uploading your configuration"
			command=$(tinyprog -p hardware.bin)
			item="No port was specified and no active bootloaders found"
			if grep -q "$item" <<< "$command"; then
				echo -e "${RED}⬤${NC} No port was specified and no active bootloaders found."
				echo -e "${RED}⬤${NC} Activate bootloader by pressing the reset button."
			fi
		fi
	else
		apio verify
	fi
elif [ "$usr_input" = "clean" ]; then
	command=$(apio clean)
	echo -e "${GREEN}⬤${NC} removed"
	echo $(pwd)
	echo "└hardware.bin"
	echo "└hardware.asc"
	echo "└hardware.json"
elif [ "$usr_input" = "boot" ]; then
	command=$(tinyprog -b)
	item="No port was specified and no active bootloaders found"
	if grep -q "$item" <<< "$command"; then
		echo -e "${RED}⬤${NC} No port was specified and no active bootloaders found."
		echo -e "${RED}⬤${NC} Activate bootloader by pressing the reset button."
	fi
elif [ "$usr_input" = "build" ]; then
	command=$(apio build)
	item="SUCCESS"
	if grep -q "$item" <<< "$command"; then
		echo -e "${GREEN}⬤${NC} SUCCESS"
	else
		apio build
	fi
elif [ "$usr_input" = "meta" ]; then
	tinyprog -m
elif [ "$usr_input" = "upload-n" ]; then
	command=$(tinyprog -p "$xusr_input")
	item="Success"
	if grep -q "$item" <<< "$command"; then
		echo -e "${GREEN}⬤${NC} your configuration has been uploaded"
	else
		echo -e "${RED}⬤${NC}there has been an error uploading your configuration"
		command=$(tinyprog -p "$xusr_input")
		item="No port was specified and no active bootloaders found"
		if grep -q "$item" <<< "$command"; then
			echo -e "${RED}⬤${NC} No port was specified and no active bootloaders found."
			echo -e "${RED}⬤${NC} Activate bootloader by pressing the reset button."
		fi
	fi
elif [ "$usr_input" = "test" ]; then
	command=$(apio verify)
	item="SUCCESS"
	if grep -q "$item" <<< "$command"; then
		echo -e "${GREEN}⬤${NC} No errors"
	else
		apio verify
	fi
elif [ "$usr_input" = "setup" ] || [ "$usr_input" = "init" ]; then
	command=$(apio init -b "$xusr_input")
	item_error="Error"
	item_no_board="Usage"
	item_success="successfully"
	if grep -q "$item_error" <<< "$command"; then
		echo "$command"
	elif grep -q "$item_no_board" <<< "$command"; then
		echo -e "${RED}⬤${NC} please supply a board name"
	elif grep -q "$item_success" <<< "$command"; then
		echo "$command"
	else
		echo -e "${RED}⬤${NC} unknown error"
	fi
elif [ "$usr_input" = "rm" ]; then
	command=$(rm apio.ini)
	echo -e "${GREEN}⬤${NC} deleted apio.ini"
elif [ "$usr_input" = "help" ]; then
	echo "help: shows this menu"
	echo "build: compiles your verilog file to the bit stream"
	echo "upload: compiles your verilog file and loads it on your fpga"
	echo "upload-n: uploads your bitstream to your fpga"
	echo "boot: boots your fpga (runs your already uploaded configuration)"
	echo "clean: deletes all files that come with the compilation of the bit stream"
	echo "meta: prints the metadata of your fpga"
	echo "test: checks your verilog file for errors"
	echo "setup/init: creates the apio.ini file needed to build the bit stream"
	echo "rm: removes the apio.ini file"
else
	echo -e "${RED}⬤${NC} wrong input, try 'fpga help' for help"
fi
